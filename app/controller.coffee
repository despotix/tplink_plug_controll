log = console.log.bind console
lerr = console.error.bind console

{ Client } = require('tplink-smarthome-api')

client = new Client()
client.startDiscovery().on 'device-new', (device)->
  name = null
  start_ts = Date.now()
  busy = false
  setInterval ->
    if busy
      return null
    busy = true
    device.getInfo()
    .then (info)->
      { power } = info?.emeter?.realtime or {}
      name = info?.sysInfo?.alias
      expected_power = ~~info?.sysInfo?.alias?.split?('/')?[1]
      if Date.now() - start_ts > 10 * 60 * 1000
        diff_percent = Math.trunc( 100 * ( power - expected_power ) / expected_power )
        if diff_percent < -20
          start_ts = Date.now()
          return device
            .setPowerState(false)
            .then ->
              log name, 'turn off', {expected_power, power} #, info
      is_on = 1 == info?.sysInfo?.relay_state
      if not is_on
        return device
          .setPowerState(true)
          .then ->
            start_ts = Date.now()
            log name, {expected_power, power}, 'turn on!'
    .catch (e)->
      lerr name, e
    .finally ->
      busy = false
  , 1*60*1000
